FROM openjdk:16-alpine3.13
COPY target/category-foundation-0.0.1-SNAPSHOT.jar category-foundation-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/category-foundation-0.0.1-SNAPSHOT.jar"]
EXPOSE 8083
