package com.burak.categoryfoundation;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;
import com.burak.categoryfoundation.domain.service.CategoryLoadingService;
import com.burak.categoryfoundation.domain.service.CategoryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.reactive.WebFluxAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
//@TestPropertySource("classpath:application-test.properties")
@TestPropertySource(locations= "classpath:application-test.yml")
@ActiveProfiles("test")
@AutoConfigureMockMvc
@WebAppConfiguration
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)


public class IntegrationTest {
    private MockMvc mockMvc;
    CategoryService categoryService;
    CategoryLoadingService categoryLoadingService;

    @Autowired
    public IntegrationTest(MockMvc mockMvc, CategoryService categoryService, CategoryLoadingService categoryLoadingService) {
        this.mockMvc = mockMvc;
        this.categoryService = categoryService;
        this.categoryLoadingService = categoryLoadingService;
    }



    //    @BeforeEach
//    public void setup() throws Exception {
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
//    }
    @Order(1)
    @Test
    void createCategories(){
        for (int i = 0; i < 30; i++) {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setId(i+1);
            categoryDto.setCategoryName("Category no: "+(i+1));
            categoryService.createCategory(categoryDto);
        }
    }
    public static String asJsonString(CategoryDto categoryDto) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(categoryDto);
        return requestJson;

    }
    @Order(2)
    @Test
    public void givenGreetURIWithQueryParameter_whenMockMVC_thenResponseOK() throws Exception {
        this.mockMvc.perform(MockMvcRequestBuilders.get("/category/get-category-by-id/6")).andDo(print()).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.categoryName").value("Category no: 6"));
    }
    @Order(3)
    @Test
    public void get() throws Exception {

        this.mockMvc.perform(MockMvcRequestBuilders.get("/category/get-category-by-id/6")).andDo(print());
    }
    @Order(4)
    @Test
    public void post() throws Exception {
        //createCategories();
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(40L);
        categoryDto.setCategoryName("Category no: 40");

        System.out.println(asJsonString(categoryDto));
        this.mockMvc.perform(MockMvcRequestBuilders.post("/category").contentType(APPLICATION_JSON).content(asJsonString(categoryDto))).andDo(print());
        System.out.println("--------------------------------------INTENTIONALLY SPACED----------------------------------------");
        this.mockMvc.perform(MockMvcRequestBuilders.get("/category/get-category-by-id/40")).andDo(print());


    }
    @Order(5)
    @Test
    public void update() throws Exception {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setId(6L);
        categoryDto.setCategoryName("Furniture");

        System.out.println(asJsonString(categoryDto));
        this.mockMvc.perform(MockMvcRequestBuilders.put("/category").contentType(APPLICATION_JSON).content(asJsonString(categoryDto))).andDo(print());
        System.out.println("--------------------------------------INTENTIONALLY SPACED----------------------------------------");
        this.mockMvc.perform(MockMvcRequestBuilders.get("/category/get-category-by-id/6")).andDo(print());
    }
    @Order(6)
    @Test
    void delete() throws Exception {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto =  categoryLoadingService.getCategoryById(6L);
        System.out.println(asJsonString(categoryDto));
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/category").contentType(APPLICATION_JSON).content(asJsonString(categoryDto))).andDo(print());
        System.out.println("--------------------------------------INTENTIONALLY SPACED----------------------------------------");
        this.mockMvc.perform(MockMvcRequestBuilders.get("/category/get-category-by-id/6")).andDo(print());

    }




}
