package com.burak.categoryfoundation;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;
import com.burak.categoryfoundation.domain.service.CategoryLoadingService;
import com.burak.categoryfoundation.domain.service.CategoryService;
import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;
//
//@ContextConfiguration(
//        classes = { DatabaseTestConfig.class},
//        loader = AnnotationConfigContextLoader.class)
//@DataJpaTest
//@Transactional
//@ContextConfiguration(classes = DatabaseTestConfig.class)
@SpringBootTest
//@TestPropertySource("classpath:application-test.properties")
@TestPropertySource(locations= "classpath:application-test.yml")
@ActiveProfiles("test")

//@PropertySource("classpath:application-test.properties")

//@RunWith(SpringRunner.class)
public class CategoryServiceImplTest {

//    private static final String CREATE_CATEGORY= "Category_t.sql";
//    private static final String DROP_CATEGORY= "DropCategory.sql";
//
//    @Autowired
//    JdbcTemplate jdbcTemplate;
//
//    @BeforeEach
//    public void before() throws ScriptException, SQLException {
//        ScriptUtils.executeSqlScript(jdbcTemplate.getDataSource().getConnection(), new ClassPathResource(CREATE_CATEGORY));
//    }
//    @AfterEach
//    public void after() throws ScriptException, SQLException {
//        ScriptUtils.executeSqlScript(jdbcTemplate.getDataSource().getConnection(), new ClassPathResource(DROP_CATEGORY));
//    }

    @Autowired
    CategoryService categoryService;
    @Autowired
    CategoryLoadingService categoryLoadingService;

//    @Test
//    public void test(){
//        CategoryDto categoryDto = new CategoryDto();
//        categoryDto.setCategoryName("Clocks");
//        categoryDto.setId(6);
//        System.out.println(categoryLoadingService.getCategoryById(6).getCategoryName());
//        categoryService.deleteCategory(categoryDto);
//        categoryService.createCategory(categoryDto);
//        System.out.println(categoryLoadingService.getCategoryById(6).getCategoryName()) ;
//    }

    void createCategories(){
        for (int i = 0; i < 30; i++) {
            CategoryDto categoryDto = new CategoryDto();
            categoryDto.setId(i+1);
            categoryDto.setCategoryName("Category no: "+(i+1));
            categoryService.createCategory(categoryDto);
        }
    }
    @Test
    void createCategoriesThenGetThemByIds(){
        createCategories();
        for (int i = 0; i < 30; i++) {
            System.out.println(categoryLoadingService.getCategoryById(i+1).getCategoryName());

        }
    }


}
