package com.burak.categoryfoundation.domain.service;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;

public interface CategoryService {
    void createCategory(CategoryDto categoryDto);
    void deleteCategory(CategoryDto categoryDto);
    void updateCategory(CategoryDto categoryDto);
    void deleteCategoryById(long id);
}
