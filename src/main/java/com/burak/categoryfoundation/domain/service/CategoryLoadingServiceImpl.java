package com.burak.categoryfoundation.domain.service;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;
import com.burak.categoryfoundation.domain.repository.CategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryLoadingServiceImpl implements CategoryLoadingService{

    @Autowired
    CategoryDao categoryDao;
    @Transactional(readOnly = true)
    @Override
    public CategoryDto getCategoryById(long id) {
        return CategoryMapper.entityToDto(categoryDao.getCategoryById(id));
    }
    @Transactional(readOnly = true)
    @Override
    public CategoryDto getCategoryByName(String name) {
        return CategoryMapper.entityToDto(categoryDao.getCategoryByName(name));
    }
}
