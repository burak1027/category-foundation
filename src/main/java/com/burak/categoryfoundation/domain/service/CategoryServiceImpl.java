package com.burak.categoryfoundation.domain.service;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;
import com.burak.categoryfoundation.domain.repository.CategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    CategoryDao categoryDao;
    @Transactional
    @Override
    public void createCategory(CategoryDto categoryDto) {
        categoryDao.addCategory(CategoryMapper.dtoToEntity(categoryDto));
    }
    @Transactional
    @Override
    public void deleteCategory(CategoryDto categoryDto) {
        categoryDao.deleteCategory(CategoryMapper.dtoToEntity(categoryDto));

    }
    @Transactional
    @Override
    public void updateCategory(CategoryDto categoryDto) {
        categoryDao.updateCategory(CategoryMapper.dtoToEntity(categoryDto));

    }

    @Override
    public void deleteCategoryById(long id) {
        categoryDao.deleteById(id);
    }
}
