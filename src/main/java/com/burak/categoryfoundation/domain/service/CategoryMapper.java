package com.burak.categoryfoundation.domain.service;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;
import com.burak.categoryfoundation.domain.model.entity.CategoryEntity;

public class CategoryMapper {
    static CategoryEntity dtoToEntity(CategoryDto categoryDto){
        if(categoryDto==null)
            return null;
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setCategoryName(categoryDto.getCategoryName());
        categoryEntity.setId(categoryDto.getId());
        categoryEntity.setDeleted(categoryDto.isDeleted());
        return categoryEntity;
    }
    static CategoryDto entityToDto(CategoryEntity categoryEntity){
        if(categoryEntity==null)
            return null;
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setCategoryName(categoryEntity.getCategoryName());
        categoryDto.setId(categoryEntity.getId());
        categoryDto.setDeleted(categoryEntity.isDeleted());
        return categoryDto;
    }
}
