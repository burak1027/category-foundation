package com.burak.categoryfoundation.domain.service;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;

import java.util.List;

public interface CategoryLoadingService {
    CategoryDto getCategoryById(long id);
    CategoryDto getCategoryByName(String name);
}
