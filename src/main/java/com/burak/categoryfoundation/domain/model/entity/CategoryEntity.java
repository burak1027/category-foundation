package com.burak.categoryfoundation.domain.model.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Category")
@Setter
@Getter
@NoArgsConstructor
@SQLDelete(sql = "UPDATE Category SET deleted = true WHERE category_id=?")
@Where(clause = "deleted=false")
public class CategoryEntity {
    @Id
    @Column(name = "category_id")
    private long id;
    @Column(name = "name")
    private String categoryName;
    @Column(name = "deleted")
    private boolean deleted;
}
