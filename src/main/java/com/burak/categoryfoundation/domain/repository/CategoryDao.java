package com.burak.categoryfoundation.domain.repository;

import com.burak.categoryfoundation.domain.model.entity.CategoryEntity;

import java.util.List;

public interface CategoryDao {
    void addCategory(CategoryEntity categoryEntity);
    void deleteCategory(CategoryEntity categoryEntity);
    void updateCategory(CategoryEntity categoryEntity);
    void deleteById(long id);

    CategoryEntity getCategoryById(Long id);
    CategoryEntity getCategoryByName(String name);
}
