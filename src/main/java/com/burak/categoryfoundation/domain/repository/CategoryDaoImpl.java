package com.burak.categoryfoundation.domain.repository;

import com.burak.categoryfoundation.domain.model.entity.CategoryEntity;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class CategoryDaoImpl implements CategoryDao{

    @Autowired
    EntityManager entityManager;

    @Override
    public void addCategory(CategoryEntity categoryEntity) {
        Session session = entityManager.unwrap(Session.class);
        session.save(categoryEntity);

    }

    @Override
    public void deleteCategory(CategoryEntity categoryEntity) {
        Session session = entityManager.unwrap(Session.class);
        session.remove(session.contains(categoryEntity) ? categoryEntity : session.merge(categoryEntity));
        //session.delete(categoryEntity);
    }

    @Override
    public void updateCategory(CategoryEntity categoryEntity) {
        Session session = entityManager.unwrap(Session.class);
        session.update(categoryEntity);
    }

    @Override
    public void deleteById(long id) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery(" UPDATE FROM CategoryEntity SET deleted = false WHERE id = :param");
        query.setParameter("param", id);
        query.executeUpdate();
    }

    @Override
    public CategoryEntity getCategoryById(Long id) {
        Session session = entityManager.unwrap(Session.class);
        return session.get(CategoryEntity.class,id);
    }

    @Override
    public CategoryEntity getCategoryByName(String name) {
        Session session = entityManager.unwrap(Session.class);
        Query query = session.createQuery("FROM CategoryEntity where categoryName = :param");
        query.setParameter("param", name);
        List<CategoryEntity> list = query.list();
        return list.get(0);
    }
}
