package com.burak.categoryfoundation.domain.controller;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;
import com.burak.categoryfoundation.domain.service.CategoryLoadingService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/category")
@Api(value = "categoryLoading", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryLoadingController {

    @Autowired
    CategoryLoadingService categoryLoadingService;

    @GetMapping("/get-category-by-id/{value}")
    CategoryDto getCategoryById(@PathVariable("value") Long id){
        return categoryLoadingService.getCategoryById(id);
    }
    @GetMapping("/get-category-by-name/{value}")
    CategoryDto getCategoryByName(@PathVariable("value") String name){
        return categoryLoadingService.getCategoryByName(name);
    }
}
