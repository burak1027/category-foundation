package com.burak.categoryfoundation.domain.controller;

import com.burak.categoryfoundation.domain.model.dto.CategoryDto;
import com.burak.categoryfoundation.domain.service.CategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/category")
@Api(value = "category", produces = MediaType.APPLICATION_JSON_VALUE)

public class CategoryController {

    @Autowired
    CategoryService categoryService;

    @PostMapping
    @ResponseBody
    public ResponseEntity<String> addCategory(@Valid @RequestBody CategoryDto categoryDto){
        categoryService.createCategory(categoryDto);
        System.out.println(categoryDto.getCategoryName());
        return ResponseEntity.ok("valid category");
    }

    @DeleteMapping
    @ResponseBody
    public ResponseEntity<String>  removeCategory(@Valid @RequestBody CategoryDto categoryDto){
        categoryService.deleteCategory(categoryDto);
        return ResponseEntity.ok("successfully updated");
    }
    @DeleteMapping("/delete-by-id/{value}")
    public ResponseEntity<String>  removeCategory(@Valid @Positive @PathVariable("value") long id){
        categoryService.deleteCategoryById(id);
        return ResponseEntity.ok("successfully updated");
    }

    @PutMapping
    @ResponseBody
    public ResponseEntity<String> updateCategory(@Valid @RequestBody CategoryDto categoryDto){
        categoryService.updateCategory(categoryDto);
        return ResponseEntity.ok("successfully deleted");
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


}
